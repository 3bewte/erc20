const hre = require("hardhat");
const fs = require('fs');
async function main() {
  try{

  const tokenContractFactory = await hre.ethers.getContractFactory("bewToken");
  const totalSupply = 1000000000000000000n * BigInt(1000000) * BigInt(process.env.TOTALSUPPLY_IN_MILLION);

  console.log(`Deploying token \nforwarder : ${process.env.BICONOMY_FORWARDER}\nsupply : ${process.env.TOTALSUPPLY_IN_MILLION}m`);

  const tokenContract = await tokenContractFactory.deploy(
    'BewCoin',
    'BEW',
    process.env.BICONOMY_FORWARDER,
    totalSupply
  );
  console.log("token contract deployed");

  const contracts = {
    "tokenContract" : tokenContract.address,
  };

  let data = JSON.stringify(contracts);
  fs.writeFileSync("contract_details.json",data);
} catch(err) {
  console.log(err);
}
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
