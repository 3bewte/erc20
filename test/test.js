const { expect } = require("chai");
const hre = require("hardhat");

const totalSupply = 1000000000000000000n * BigInt(1000 * 1000 * 52);
const forwarder = "0x9399BB24DBB5C4b782C70c2969F58716Ebbd6a3b";
const ipfsUrl = "https://ipfs.io/ipfs/bafyreift6sbigsst7424jyzfjh5vtwrthgxbzrvgoh5tuureyw25qcwfki/metadata.json";
let bewContract = null;
let nftContract = null;
let auctionContract = null;
let owner;
let creator;
let bidder;

describe("ERC 20 Intro testcases", async function(){
  it("BEW Contract deployment with dummy trusted forwarder", async function(){
    [owner, creator, bidder] = await ethers.getSigners();
    const contractFactory = await hre.ethers.getContractFactory("bewToken");
    bewContract = await contractFactory.deploy(
      'BewCoin', 'BEW', forwarder, totalSupply// 52 million
      );
    expect(await bewContract.balanceOf(owner.address)).to.equal(totalSupply);
  });
  it("Should transfer bews to another account", async function(){
    await bewContract.connect(owner).transfer(creator.address, BigInt(10) * 1000000000000000000n);
    expect(await bewContract.balanceOf(creator.address)).to.equal(BigInt(10) * 1000000000000000000n);
    expect(await bewContract.balanceOf(owner.address)).to.equal(totalSupply - BigInt(10) * 1000000000000000000n);
  })
})
